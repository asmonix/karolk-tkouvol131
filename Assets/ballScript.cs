﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ballScript : MonoBehaviour {

	[SerializeField]
	public Rigidbody2D rb;
	public Rigidbody2D hook;
	public float releaseTime = .15f;
	public float maxDragDistance = 2f;
	public GameObject ballPrefab;

	[HideInInspector]
	public GameObject nextBall;
	private bool isPressed = false;

	private void Start()
	{
		GameObject hookHook = GameObject.Find("Start");
		hook = hookHook.GetComponent<Rigidbody2D>();
		this.GetComponent<SpringJoint2D>().connectedBody = hook;
		
	}

	void Update ()
	{
		if (isPressed)
		{
			Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			if (Vector3.Distance(mousePos, hook.position) > maxDragDistance)
				rb.position = hook.position + (mousePos - hook.position).normalized * maxDragDistance;
			else
				rb.position = mousePos;
		}
	}

	void OnMouseDown ()
	{
		isPressed = true;
		rb.isKinematic = true;
	}

	void OnMouseUp ()
	{
		isPressed = false;
		rb.isKinematic = false;

		StartCoroutine(Release());
	}

	IEnumerator Release ()
	{
		yield return new WaitForSeconds(releaseTime);

		GetComponent<SpringJoint2D>().enabled = false;
		this.enabled = false;

		yield return new WaitForSeconds(1f);

		if (nextBall == null)
		{
			Vector3 startposition = hook.transform.position;
			nextBall = Instantiate(ballPrefab, startposition, Quaternion.identity);
			nextBall.SetActive(true);
		}
	
	}

}
